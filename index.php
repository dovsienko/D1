<?php

if ($_SERVER['REQUEST_METHOD'] == 'HEAD')
	exit;
if ($_SERVER['REQUEST_METHOD'] != 'GET')
{
	header ("${_SERVER['SERVER_PROTOCOL']} 405 Method Not Allowed");
	exit;
}

?><!DOCTYPE html>
<HTML lang="en">
	<HEAD>
<?php

/*
This script is copyright (c) 2021-2024 Denis Ovsienko
See https://gitlab.com/dovsienko/D1 for the most recent version.

All trademarks and brand names are the rightful properties of their
respective owners and are used in this software for the sole purpose
of description or identification.

SPDX-License-Identifier: GPL-3.0-only

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

$pagetitle = 'Assorted resources about the Allwinner Nezha D1 RISC-V development board';
echo "<TITLE>${pagetitle}</TITLE>\n";

?>
		<META charset="utf-8">
		<LINK rel='shortcut icon' href='D1.png'>
		<STYLE>
BODY {
	font-family: Arial, Helvetica;
}

TABLE.metadata TBODY TR {
	background: #f0f0f0;
}

TABLE.metadata TBODY TR:nth-child(even) {
	background: #e0e0e0;
}

TABLE.metadata TBODY TR.working {
	background: palegreen;
}

TABLE.metadata TBODY TR.wip {
	background: #FBFB98;
}

TABLE.metadata TBODY TR.broken {
	background: #FB9898;
}

TABLE.metadata TR {
	vertical-align: top;
}

TABLE.metadata TD, TABLE.metadata TH {
	padding: 5px;
}

SPAN.digest, SPAN.filename {
	font-family: monospace;
}

SPAN.filename {
	font-size: large;
	font-weight: bold;
}

SPAN.local_copy {
	font-size: small;
}

PRE.cli {
	padding: 5px;
	border-radius: 5px;
	font-size: large;
	background-color: black;
	color: lime;
}

SPAN.filedesc {
	font-style: italic;
}
		</STYLE>
	</HEAD>
	<BODY>
<?php

echo "<H1>${pagetitle}</H1>\n";

$headers = array
(
	'Filename and Description',
	'Packed Size',
	'Packed SHA-1',
	'Unpacked Size',
	'Unpacked SHA-1',
	'Notes',
);

$rows = array
(
	array
	(
		'description' => 'Ubuntu Server 24.04.1, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/24.04.1/release/ubuntu-24.04.1-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 894403572,
		'packed_sha1' => '41decd4ef8d4c047ec52a663db35fc7c2f1ab059',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => '54b2554d070da016989348241074ff33d2b74268',
		'footnotes' => array (13),
	),
	array
	(
		'description' => 'Ubuntu Server 24.04, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/24.04/release/ubuntu-24.04-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 885976616,
		'packed_sha1' => 'ac69b84d4b747813135f15e0d9ca8f2323eb9212',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => 'b92fc34ba8c07f7a6e8617fa51ab4dc97678154d',
		'footnotes' => array (13),
	),
	array
	(
		'status' => 'working',
		'description' => 'Ubuntu Server 22.04.4, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/22.04.4/release/ubuntu-22.04.4-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 790528876,
		'packed_sha1' => '0dac7769e4158fd316545836ba2af66b0e0454cf',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => '580239513fa651ff8bd274b95c74af6ea2bd4527',
		'footnotes' => array (13, 14),
	),
	array
	(
		'description' => 'Ubuntu Server 23.10, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/23.10/release/ubuntu-23.10-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 1033695920,
		'packed_sha1' => 'f31ee724a0248f1203e034b0184bd514bc57c431',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => '2f072ce3c5c8467101466039fb35e396859802d1',
		'footnotes' => array (13),
	),
	array
	(
		'status' => 'working',
		'description' => 'Ubuntu Server 22.04.3, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/22.04.3/release/ubuntu-22.04.3-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 775841376,
		'packed_sha1' => '87b02e0f5e70a070d701df0b4ab1ddb7650b7d71',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => 'e2361b78d4528e0160b9bae3489dd520ecc38dc1',
		'footnotes' => array (13, 14),
	),
	array
	(
		'description' => 'Ubuntu Server 23.04, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/23.04/release/ubuntu-23.04-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 811976084,
		'packed_sha1' => 'b38697d318572ef298b2439878313378b4b05587',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => '02e7fec90171b27a976fb06003d18ef7e6e28b57',
		'footnotes' => array (13),
	),
	array
	(
		'status' => 'working',
		'description' => 'Ubuntu Server 22.04.2, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/22.04.2/release/ubuntu-22.04.2-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 754729720,
		'packed_sha1' => 'd1fde1b94576d92b728b534f44b32521140da420',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => '3df9b820fc8357b5ed9ca80c9d27bfbd7a68e2f9',
		'footnotes' => array (13, 14),
	),
	array
	(
		'filename' => 'D1_boot_Ubuntu-22.04.2_complete_2023-05-04.txt',
		'description' => 'boot log for the above',
		'unpacked_size' => 'auto',
	),
	array
	(
		'description' => 'Ubuntu Server 22.10, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/22.10/release/ubuntu-22.10-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 762542840,
		'packed_sha1' => '994e5bca0b916d8a06e655804e384b6e491fb7d1',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => 'f191586649c62ad35142fdbad6153007e435a651',
		'footnotes' => array (13),
	),
	array
	(
		'status' => 'working',
		'description' => 'Ubuntu Server 22.04.1, xz-compressed plain disk image',
		'URL' => 'https://cdimage.ubuntu.com/releases/22.04.1/release/ubuntu-22.04.1-preinstalled-server-riscv64+nezha.img.xz',
		'packed_size' => 787562356,
		'packed_sha1' => 'f65ec64b9730a2dea307f5411457c15ee792c422',
		'unpacked_size' => 4831838208,
		'unpacked_sha1' => '065ec1585e06b1964d53088f4f5779115b746ce4',
		'footnotes' => array (13, 14),
	),
	array
	(
		'filename' => 'D1_boot_Ubuntu-22.04_complete_2022-10-23.txt',
		'description' => 'boot log for the above',
		'unpacked_size' => 'auto',
	),
	array
	(
		'description' => 'Fedora, zstd-compressed plain disk image',
		'URL' => 'https://openkoji.iscas.ac.cn/pub/dist-repos/old_dl/Allwinner/Nezha_D1/images-release/Fedora/fedora-disk-developer-xfce_allwinner_d1_test-F36-20220716-000826.n.0-sda.raw.zst',
		'packed_size' => 2497462045,
		'packed_sha1' => 'a81789198c1190c311a7b0c117046ded8830e605',
		'unpacked_size' => 13589544960,
		'unpacked_sha1' => '4781a8d449a497d2df0e545b791b351211c08e90',
		'footnotes' => array (6),
	),
	array
	(
		'status' => 'wip',
		'filename' => 'fedora-riscv64-d1-developer-xfce-rawhide-Rawhide-20220117-135925.n.0-sda.raw.zst',
		'URL' => 'https://www.robertlipe.com/download/fedora-images-riscv64-d1-developer-xfce-rawhide-jan-17-2022/',
		'description' => 'Fedora, zstd-compressed plain disk image',
		'mirror_URL' => 'https://openkoji.iscas.ac.cn/pub/dist-repos/old_dl/Allwinner/Nezha_D1/images-release/Fedora/fedora-riscv64-d1-developer-xfce-rawhide-Rawhide-20220117-135925.n.0-sda.raw.zst',
		'mirror_description' => 'original file',
		'packed_size' => 3060402609,
		'packed_sha1' => 'aadb8ca1fcd0b67f1f65466ab31418a1eb0ca030',
		'unpacked_size' => 13589544960,
		'unpacked_sha1' => 'f18e69e165ef5520e0b36cb93241572b60b5de9d',
		'footnotes' => array (6, 7, 8, 10, 11),
	),
	array
	(
		'filename' => 'fedora-2022-01-17_kernel_5.4.61_extlinux_manual_1GB.txt',
		'description' => 'boot log for the above Fedora image (kernel 5.4.61 via original U-Boot, extlinux and manual input)',
		'unpacked_size' => 'auto',
		'footnotes' => array (7),
	),
	array
	(
		'status' => 'working',
		'filename' => 'fedora-riscv64-d1-developer-xfce-rawhide-Rawhide-20211130-010217.n.0-sda.raw.zst',
		'URL' => 'https://www.robertlipe.com/download/fedora-images-riscv64-d1-developer-xfce-rawhide-2/',
		'description' => 'Fedora, zstd-compressed plain disk image',
		'mirror_URL' => 'https://openkoji.iscas.ac.cn/pub/dist-repos/old_dl/Allwinner/Nezha_D1/images-release/Fedora/fedora-riscv64-d1-developer-xfce-rawhide-Rawhide-20211130-010217.n.0-sda.raw.zst',
		'mirror_description' => 'original file',
		'packed_size' => 2949853513,
		'packed_sha1' => '46d9baf4c2ab47bb476cee0ec5c68ab5fcd58929',
		'unpacked_size' => 13589544960,
		'unpacked_sha1' => '5fa09af0a1af148f7fe70a97aaf028ee47899fe0',
		'footnotes' => array (6, 7, 8, 9, 10),
	),
	array
	(
		'filename' => 'fedora-2021-11-30_kernel_5.4.61_extlinux_manual_1GB.txt',
		'description' => 'boot log for the above Fedora image (kernel 5.4.61 via original U-Boot, extlinux and manual input)',
		'unpacked_size' => 'auto',
		'footnotes' => array (7),
	),
	array
	(
		'filename' => 'fedora-2021-11-30_kernel_5.16.0-rc2_manual_GRUB.txt',
		'description' => 'boot log for the above Fedora image (kernel 5.16.0-rc2 via custom U-Boot, GRUB and manual input)',
		'unpacked_size' => 'auto',
		'footnotes' => array (8),
	),
	array
	(
		'filename' => 'fedora-2021-11-30_D1-1GB_bootloader.bin',
		'description' => 'custom D1-1GB bootloader block for the above Fedora image',
		'unpacked_size' => 'auto',
		'unpacked_sha1' => '8ac5752e14ef8db5cc6d82d26e70684c05e4d4eb',
		'footnotes' => array (9),
	),
	array
	(
		'filename' => 'minicom1.cap.txt',
		'description' => 'OpenWrt boot log',
		'unpacked_size' => 'auto',
		'footnotes' => array (5),
	),
	array
	(
		'filename' => 'minicom2.cap.txt',
		'description' => 'U-Boot default environment and available commands',
		'unpacked_size' => 'auto',
		'footnotes' => array (5),
	),
);

$footnotes = array
(
	5 => <<<'ENDOFTEXT'
By Gary E. Miller.
ENDOFTEXT
,
	6 => <<<'ENDOFTEXT'
As published on
<A href='https://fedoraproject.org/wiki/Architectures/RISC-V/Allwinner'>Fedora RISC-V wiki</A> and
<A href='https://openkoji.iscas.ac.cn/pub/dist-repos/old_dl/Allwinner/Nezha_D1/images-release/Fedora/'>openkoji service</A>
of <A href='http://www.iscas.ac.cn/'>Institute of Software, Chinese Academy of Sciences</A>.
ENDOFTEXT
,
	7 => <<<'ENDOFTEXT'
The Linux kernel in this disk image does not auto-detect the RAM size, but uses
a hard-coded size from a DeviceTree file. Each boot menu item has its own
DeviceTree file, and the default boot menu item (1) stands for 2 GB:
<PRE class=cli>
Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide-20210912.n.0 Boot Options.
1:      Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (5.4.61+ 2G)
2:      Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (5.4.61+ 1G)
3:      Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (5.4.61+ 512M)
4:      Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (5.4.61+ 128M)
Enter choice: 1:        Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (5.4.61+ 2G)
[...]
Retrieving file: /aw_nezha_d1_2G.dtb
</PRE>
<STRONG>If you try to use more RAM than the D1 board actually has, it will
sooner or later crash.</STRONG>
The following commands can be used to change the default to 1 GB:
<PRE class=cli>
# For the 2021-09-12 revision:
echo 'default Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (5.4.61+ 1G)' &gt;&gt;/boot/extlinux/extlinux.conf

# For the 2021-11-30 revision:
echo 'default Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (5.4.16.riscv64.fc33.riscv64 1G)' &gt;&gt;/boot/extlinux/extlinux.conf

# For the 2022-01-17 revision:
echo 'default Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide (kernel-5.4.61 HDMI 1080p 1G RAM)' &gt;&gt;/boot/extlinux/extlinux.conf
</PRE>
ENDOFTEXT
,
	8 => <<<'ENDOFTEXT'
To boot the latest kernel, this image requires booting through GRUB. This can be done manually
during the boot sequence as follows:
<PRE class=cli>
[...]
Importing environment from mmc0 ...
Autoboot in 2 seconds  &lt;---------------------------------- press "v"
[Nezha]#
[Nezha]# run boot_grub
4681728 bytes read in 808 ms (5.5 MiB/s)
Scanning disk mmc@4020000.blk...
Scanning disk mmc@4021000.blk...
Disk mmc@4021000.blk not ready
Found 4 disks
No EFI system partition
Booting /\EFI\fedora\grubriscv64.efi
Welcome to GRUB!
[...]
</PRE>
The following command can be used after booting to have U-Boot load GRUB by default:
<PRE class=cli>
echo 'bootcmd=run boot_grub' &gt;&gt;/boot/boot/uEnv.txt
</PRE>
ENDOFTEXT
,
	9 => <<<'ENDOFTEXT'
When booting the latest kernel from this image, it will use the RAM size provided by U-Boot, not the
RAM size in DeviceTree files. However, U-Boot in this image has the RAM size hard-coded to 2GB, so
to work with the latest kernel on D1-1GB this image requires to replace the bootloader with a custom
version (provided by Fu Wei):
<PRE class=cli>
dd if=fedora-2021-11-30_D1-1GB_bootloader.bin bs=512 seek=32800 of=/dev/mmcblk0
</PRE>
ENDOFTEXT
,
	10 => <<<'ENDOFTEXT'
All files hosted
<a href="https://www.robertlipe.com/d1-as-in-nezha-lichee-rv-mango-pi-pro-disk-images-and-info/">here</a>
are stored as ZIP archives, so if the original file is not
a ZIP archive, the mirror copy has an extra ZIP container around it.  This way, a
<span class=filename>.zip</span> file is stored as is, but a <span class=filename>.raw.zst</span>
file becomes a <span class=filename>.raw.zst.zip</span> file, the "Packed Size" column refers to
the <span class=filename>.raw.zst</span> file and the "Unpacked Size" column refers to the
<span class=filename>.raw</span> file, and so on.
ENDOFTEXT
,
	11 => <<<'ENDOFTEXT'
In this image U-boot is hard-coded to 2GB RAM and there is no replacement version as of time of this
writing. Also kernel 5.4.61 does not detect the MTD.  Also GRUB defaults to the 2GB RAM menu entry
and uses incorrect DeviceTree file paths.  The error messages about the paths can be squelched using
the commands below:
<PRE class=cli>
sed --in-place=.orig 's/\/dtbs\/5.16.0+\//\/5.16.0-rc2+\//g' /boot/grub.cfg
# 0 is the default (2GB), 1 is 1GB
sed --in-place 's/^set default=0/set default=1/' /boot/grub.cfg
</PRE>
However, kernel 5.16.0 in this disk image disregards DeviceTree data anyway and boots assuming 2GB RAM
(possibly using the memory size from U-boot).
<strong>In all likelihood this disk image cannot boot kernel 5.16.0 on D1-1GB.</strong>
ENDOFTEXT
,
	13 => <<<'ENDOFTEXT'
The file is available for download <A href='https://ubuntu.com/download/risc-v'>
here</A>, additional information is available
<A href='https://discourse.ubuntu.com/t/ubuntu-on-the-visionfive-and-the-nezha-boards/29858'>
here</A>.
ENDOFTEXT
,
	14 => <<<'ENDOFTEXT'
This image by default omits much of the boot progress from the serial
console (Ubuntu bug
<A href='https://bugs.launchpad.net/ubuntu/+source/livecd-rootfs/+bug/1994072'>
1994072</A>). Clang 14 package was broken (Ubuntu bug
<A href='https://bugs.launchpad.net/ubuntu/+source/llvm-defaults/+bug/1994071'>
1994071</A>), but has been fixed now.
Updating the GRUB packages makes the system unbootable (Ubuntu bug
<A href='https://bugs.launchpad.net/ubuntu/+source/grub/+bug/2011744'>
2011744</A>), which can be worked around as follows:
<PRE class=cli>
dpkg --set-selections &lt;&lt;EOF
grub-common hold
grub-efi-riscv64-bin hold
grub-efi-riscv64 hold
grub2-common hold
u-boot-nezha hold
EOF
</PRE>
ENDOFTEXT
,
);

?>
		<H2>Some useful links</H2>
		<UL>
			<LI><A href='https://d1.docs.aw-ol.com/en/'>Allwinner documentation</A></LI>
			<LI><A href='https://linux-sunxi.org/Allwinner_Nezha'>sunxi wiki: Allwinner Nezha page</A></LI>
			<LI><A href='https://linux-sunxi.org/D1'>sunxi wiki: D1 page</A></LI>
			<LI><A href='https://github.com/rvboards/d1_demo'>RVBoards D1 demos</A></LI>
			<LI>
				<A href='https://www.rvboards.org/mkdocs/en/nezha-d1/'>RVBoards D1 documentation</A>
				(<A href='https://github.com/rvboards/d1_nezha_doc'>source</A>)
			</LI>
			<LI><A href='https://groups.google.com/a/riscv.org/group/devboard-community/'>riscv.org devboard-community mailing list</A></LI>
			<LI><A href='D1_pinout.jpeg'>D1 40-pin header pin-out label</A></LI>
			<LI>
				Fedora RISC-V wiki pages: <A href='https://fedoraproject.org/wiki/Architectures/RISC-V/Allwinner'>D1-specific information</A>,
				<A href='https://fedoraproject.org/wiki/Architectures/RISC-V/Installing'>generic installation guide</A>
			</LI>
			<LI><A href='https://github.com/xboot/xfel'>Tiny FEL tools for Allwinner SoC</A></LI>
			<LI><A href='https://github.com/marsfan/d1-buildroot'>D1 buildroot for Docker containers</A></LI>
			<LI><A href='https://github.com/YuzukiTsuru/OpenixCard'>OpenixCard free software for Allwinner PhoenixCard images</A></LI>
			<LI><A href='https://gitlab.com/dovsienko/D1'>Source code of this page</A></LI>
		</UL>
		<H2>Hardware</H2>
			<H3>Power supply</H3>
			<P>
			Robert Lipe
			<A href="https://www.robertlipe.com/usb-c-power-issues-with-development-boards/">explains</A>
			why Apple hardware and Nezha D1 do not work together using a
			USB-C to USB-C cable. It should be possible to work around the problem using the
			USB-A to USB-C cable(s) supplied with the board.
			</P>
			<H3>SPI NAND layout</H3>
			<P>Kernel 5.4.61 (as reported by Gabe R):</P>
<PRE class=cli>
# cat /proc/mtd
dev:    size   erasesize  name
mtd0: 00100000 00040000 "boot0"
mtd1: 00400000 00040000 "uboot"
mtd2: 00100000 00040000 "secure_storage"
mtd3: 0fa00000 00040000 "sys"
</PRE>
			<P>
			In <A href="https://groups.google.com/a/riscv.org/g/devboard-community/c/4ZSgwzMr4nU">this
			thread</A> Samuel Holland notes that this partitioning is an effect of Allwinner firmware,
			which is not compatible with upstream Linux.
			</P>
			<P>Kernel 5.16.0-rc2:</P>
<PRE class=cli>
# cat /proc/mtd
dev:    size   erasesize  name
mtd0: 10000000 00020000 "spi0.0"
</PRE>
			<P>
			The same stands for kernels in the Ubuntu 22.04 image below.
			</P>
			<H3>Serial console</H3>
			<P>
			The USB serial adapter that comes with the board connects to the 3-pin "DEBUG" header on
			the board as follows:
			</P>
			<UL>
				<LI>black&mdash;GND pin</LI>
				<LI>green&mdash;RX pin</LI>
				<LI>white&mdash;TX pin</LI>
				<LI>red&mdash;do not connect</LI>
			</UL>
			<P>Peter Gutmann notes that the supplied serial cable can be faulty.  If this is the case,
			it needs to be replaced with a serial adapter that <strong>uses 3.3V levels</strong>.
			</P>
			<H3>RGB LED</H3>
			<P>
			With kernel 5.16.0 (which is available in some disk images and does not boot by default)
			it is possible to control the RGB LED of the board, which is labelled "LED" and sits next
			to the red "PWR" LED:
			</P>
<PRE class=cli>
LED=/sys/class/leds/rgb:indicator
echo 0 50 50 &gt;"$LED"/multi_intensity
echo mmc0 &gt;"$LED"/trigger
</PRE>
			<P>
			The same stands for 5.19.x kernels in the Ubuntu 22.04 images below,
			in 6.5.x kernels the sysfs path changed to
			<code>/sys/class/leds/rgb:status</code>.
			</P>
			<H3>Temperature sensor</H3>
			<P>
			Some kernels (at least those in Ubuntu 22.04 disk image) support the temperature sensor
			built into the board/chip:
			</P>
<PRE class=cli>
# sensors
cpu_thermal-virtual-0
Adapter: Virtual device
temp1:        +32.2°C  (crit = +110.0°C)
</PRE>
		<H2>Some files (may be mirrored, or not)</H2>
	  <TABLE class=metadata>
		<THEAD>
			<TR>
<?php
foreach ($headers as $header)
	echo "<TH>${header}</TH>";
?>
			</TR>
		</THEAD>
		<TBODY>
<?php

define ('ONE_MiB', (2 ** 10) ** 2);
define ('ONE_MB', 10 ** 6);
define ('ONE_GiB', (2 ** 10) ** 3);
define ('ONE_GB', 10 ** 9);

function size_str (int $x): string
{
	$ret = sprintf ('%u %s', $x, $x % 10 == 1 ? 'byte' : 'bytes');
	if ($x > ONE_MiB)
	{
		if ($x > ONE_GiB)
		{
			$bin = sprintf ('%.2f GiB', $x / ONE_GiB);
			$dec = sprintf ('%.2f GB', $x / ONE_GB);
		}
		else
		{
			$bin = sprintf ('%.2f MiB', $x / ONE_MiB);
			$dec = sprintf ('%.2f MB', $x / ONE_MB);
		}
		$ret .= "<BR>(${bin}, ${dec})";
	}
	return $ret;
}

function get_filename (array $row): string
{
	if (array_key_exists ('filename', $row))
		return $row['filename'];
	if (array_key_exists ('URL', $row))
		return preg_replace ('#^.+/([^/]+)$#', '\\1', $row['URL']);
	throw new Exception ('malformed file entry');
}

function auto_size_str ($size, string $filename): string
{
	if (is_int ($size))
		return size_str ($size);
	if ($size === 'auto')
	{
		if (! file_exists ($filename))
			throw new Exception ("File '${filename}' does not exist.");
		$stat = stat ($filename);
		return size_str ($stat['size']);
	}
	throw new Exception ("Invalid size '${size}'");
}

function footnotes_str (array $footnotes): string
{
	$tmp = array();
	foreach ($footnotes as $note_num)
		$tmp[] = "<A href='#fn${note_num}'>${note_num}</A>";
	return implode (', ', $tmp);
}

foreach ($rows as $row)
{
	$filename = get_filename ($row);
	$href = array_key_exists ('URL', $row) ? $row['URL'] : $filename;
	$filename_description = "<A href='${href}'><SPAN class=filename>${filename}</SPAN></A>\n";
	if (array_key_exists ('description', $row))
		$filename_description .= "<BR><SPAN class=filedesc>${row['description']}</SPAN>\n";
	if (array_key_exists ('mirror_URL', $row) && array_key_exists ('mirror_description', $row))
		$filename_description .= "<BR><SPAN class=local_copy>(<A href='${row['mirror_URL']}'>" .
			"${row['mirror_description']}</A>)</SPAN>";
	elseif (array_key_exists ('URL', $row) && file_exists ($filename))
		$filename_description .= "<BR><SPAN class=local_copy>(<A href='${filename}'>local copy</A>)</SPAN>";
	$columns = array
	(
		$filename_description,
		array_key_exists ('packed_size', $row) ? auto_size_str ($row['packed_size'], $filename) : NULL,
		array_key_exists ('packed_sha1', $row) ? "<SPAN class=digest>${row['packed_sha1']}</SPAN>" : NULL,
		array_key_exists ('unpacked_size', $row) ? auto_size_str ($row['unpacked_size'], $filename) : NULL,
		array_key_exists ('unpacked_sha1', $row) ? "<SPAN class=digest>${row['unpacked_sha1']}</SPAN>" : NULL,
		array_key_exists ('footnotes', $row) ? footnotes_str ($row['footnotes']) : NULL,
	);
	echo '<TR' . (array_key_exists ('status', $row) ? " class=${row['status']}" : '') . ">\n";
	foreach ($columns as $column)
		echo '<TD>' . ($column ?? '&nbsp;') . "</TD>\n";
	echo "</TR>\n";
}
?>
		</TBODY>
		</TABLE>
		<H2>Notes</H2>
		<DL>
<?php
foreach ($footnotes as $note_num => $note_text)
{
	echo "<DT id=fn${note_num}>${note_num}</DT>\n";
	echo "<DD>${note_text}</DD>";
}
?>
		</DL>
	</BODY>
</HTML>
